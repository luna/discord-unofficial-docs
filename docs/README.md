# discord-unofficial-docs

Unofficial documentation for Undocumented Discord APIs. Also contains an outline
of Discord's infrastructure.

[Source repository link here](https://gitlab.com/luna/discord-unofficial-docs)

## Table of Contents

- [Attachments](/docs/attachments)
- [Stickers](/docs/stickers)
- [Relationships](/docs/relationships)
- [Connection Properties](/docs/connection_properties)
- [Clyde Settings](/docs/clyde_settings)
- [Mobile Indicator](/docs/mobile_indicator)
- [Lazy Guilds](/docs/lazy_guilds)
- [User Data Collection](/docs/science)
  - [Event List](/docs/science_events)
- [Per-Client Statuses](/docs/per-client_status)
- [Guild Features](/docs/guild_features)
- [Discord's Infrastructure](/docs/infrastructure)
- [Guild Sync](/docs/guild_sync)
- [Guild Folders](/docs/guild_folders)
- [Friend Invites](/docs/friend_invite)
- [User Notes](/docs/user_notes)
- [User Settings](/docs/user_settings)
- [Remote Auth (Desktop)](/docs/desktop_remote_auth)
- [Remote Auth V2 (Desktop)](/docs/desktop_remote_auth_v2)
- [Remote Auth (Mobile)](/docs/mobile_remote_auth)
- [Custom Status](/docs/custom_status)
- [Client-side Interactions](/docs/client_side_interactions)
- [Integrations](/docs/integrations)
- [Android's minimal version check](/docs/android/min_version)

## Licensing?

![CC0 logo](https://i.creativecommons.org/p/zero/1.0/88x31.png)

To the extent possible under law, [Luna Mendes](https://l4.pm) has waived all
copyright and related or neighboring rights to the Unofficial Discord API
Documentation. This work is published from Brazil.
