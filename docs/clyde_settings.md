# Clyde Settings

You can set Clyde's personality if you have him enabled in your server with a secret API route.

## PATCH /guilds/{guild.id}/clyde-settings
### Arguments
| **Name** | **Type** | **Description** |
|---|---|---|
| personality | string | String that defines clyde's personality and how he should act in the server. Must be 500 words or less. |
