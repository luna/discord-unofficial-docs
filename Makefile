
check:
	./check_links.py

build: build/docs/index.html

build/docs/index.html:
	npm run build

clean:
	rm -rv ./build/

dev:
	npm run start
