// @ts-check
// `@type` JSDoc annotations allow editor autocompletion and type checking
// (when paired with `@ts-check`).
// There are various equivalent ways to declare your Docusaurus config.
// See: https://docusaurus.io/docs/api/docusaurus-config

import {themes as prismThemes} from 'prism-react-renderer';

function orderItems(titleMap, items) {
  const result = items.map((item) => {
    if (item.type === 'category') {
      return {...item, items: orderItems(titleMap, item.items)};
    }
    return item;
  });
  result.sort((a, b) =>
  (titleMap[a.id] || a.label).localeCompare(
    titleMap[b.id] || b.label,
    "en",
    { sensitivity: "base" },
  ),
);

  result.sort((a, b) => {
    const aName = (titleMap[a.id] || a.label);
    const bName = (titleMap[b.id] || b.label);
    return aName.localeCompare(bName, "en", { sensitivity: "base" });
  });
  return result;
}

async function sidebarItemsGenerator({defaultSidebarItemsGenerator, ...args}) {
  let titleMap = {};
  args.docs.map((doc) => titleMap[doc.id] = doc.title);
  const sidebarItems = await defaultSidebarItemsGenerator(args);
  return orderItems(titleMap, sidebarItems);
}

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'discord-unofficial-docs',
  tagline: 'Unoffical Documentation for the Discord API',
  favicon: 'img/favicon.ico',

  // Set the production url of your site here
  url: 'https://luna.gitlab.io',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/discord-unofficial-docs',

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'throw',
  markdown: {
    format: 'md',
  },

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: './sidebars.js',
          sidebarItemsGenerator: sidebarItemsGenerator,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/luna/discord-unofficial-docs',
        },
        blog: {
          showReadingTime: true,
        },
        theme: {
          customCss: './src/css/custom.css',
        },
      }),
    ],
  ],

  
  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: "discord-unofficial-docs",
        items: [
          {
            type: "docSidebar",
            sidebarId: "sidebar",
            position: "left",
            label: "Docs"
          },
          {
            href: "https://gitlab.com/luna/discord-unofficial-docs",
            label: "Gitlab",
            position: "right"
          }
        ]
      },

      prism: {
        theme: prismThemes.github,
        darkTheme: prismThemes.dracula,
      }
    })
};

export default config;
